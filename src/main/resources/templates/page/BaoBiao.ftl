<html>
<head>
    <meta charset="UTF-8" />
    <title>您的消费视图</title>

    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/meetingSign/js/charts.js"></script>
    <link rel="stylesheet" href="/meetingSign/css/pintuer.css">
</head>
<body>
<div id="container" style="width: 550px; height: 400px; margin: 0 auto"></div>
<script language="JavaScript">
    $(document).ready(function() {
        var chart = {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        };
        var title = {
            text: '您开始记录的消费总百分比'
        };
        var tooltip = {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        };
        var plotOptions = {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        };
        var series= [{
            type: 'pie',
            name: '消费占总百分比',
            data: [
        <#list ListMoneyData as money>
            <#if money.allCount gt 0>
            ['${money.name} ',  ${money.baifenbi}],
            </#if>
        </#list>
            ]
        }];

        var json = {};
        json.chart = chart;
        json.title = title;
        json.tooltip = tooltip;
        json.series = series;
        json.plotOptions = plotOptions;
        $('#container').highcharts(json);
    });


</script>


<br>
<blockquote>
    <p>您的总记录为：<span style="font-size: 24px">${count}</span>条<br></p>
    <p>总记录消费金额为：<span style="font-size: 24px">${sellCount}</span> 元</p>
</blockquote>

<table class="table table-bordered">
    <tr>
        <th>
             消费名称
        </th>
        <th>
            花费价格
        </th>
        <th>
            所占比例
        </th>
    </tr>
    <#list ListMoneyData as money>
        <#if money.allCount gt 0>
         <tr>
             <td>
                 ${money.name}
             </td>
             <td>
                 ${money.allCount}元
             </td>
             <td>
                 <#assign x = money.baifenbi>
                 ${x * 100}%
             </td>
         </tr>
        </#if>

    </#list>

</table>
</body>
</html>