<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>消费记录</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" href="/meetingSign/css/pintuer.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/weui/1.1.2/style/weui.min.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/jquery-weui/1.2.0/css/jquery-weui.min.css">
    <script src="/meetingSign/js/jquery.js"></script>
<#--<link rel="stylesheet" type="text/css" href="/meetingSign/css/datedropper.css">-->
<#--<link rel="stylesheet" type="text/css" href="/meetingSign/css/timedropper.min.css">-->
<#--<script>-->
<#--window.onload=function(){-->
<#--$.alert("自定义的消息内容");-->
<#--}-->
<#--</script>-->
</head>
<body>
<input value="${size}" hidden id="size">
<input value="${page}" hidden id="page">
<div class="alert alert-blue" style="text-align: center;font-size: 13px">
    点击头像可返回上一页面
</div>
<div id="main" style="text-align: center">
    <a href="/meetingSign/jizhang/index?openid=${openid}&imgHead=${imgHead}"><img src="${imgHead}" width="100" height="100" class="img-border radius-circle"style="margin: 0px;text-align: center"/></a>
</div>
<br>
<div class="weui-cells">
    <a class="weui-cell weui-cell_access" onclick="baobiao();">
        <div class="weui-cell__bd">
            <p>点击我获取报表—>总消费:￥${sellCount!""}</p>
        </div>
        <div class="weui-cell__ft">
        </div>
    </a>
</div>
<div id="tableDiv"></div>
<br>


<div id="pageJizhang"></div>
</body>
<!-- body 最后 -->
<script src="https://cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/jquery-weui/1.2.0/js/jquery-weui.min.js"></script>
<script>
    window.onload = function() {
        $.showLoading();
        setTimeout(function () {
            $.hideLoading();
        },500);
        // document.getElementById("submitForm").onclick = function () {
        //     var choseTime =  document.getElementById("choseTime").value;
        //     var eventContent = document.getElementById("eventContent").value;
        //     var money = document.getElementById("money").value;
        //     var picker = document.getElementById("picker").value;
        //     if(choseTime.length<=0){
        //         $.alert("时间不能为空");
        //         return;
        //     }
        //     if(money.length<=0){
        //         $.alert("请填写消费的金额");
        //         return;
        //     }
        //     if(picker.length<=0){
        //         $.alert("请选择消费类型");
        //         return;
        //     }
        //     document.getElementById("eventForm").submit();
        // }

    }
    function baobiao() {
        $.get("/meetingSign/jizhang/getBaoBiao", function (data, status) {
                $.alert("发送成功，请注意查收");
        });
    }
    function onlyNumber(obj){
        //得到第一个字符是否为负号
        var t = obj.value.charAt(0);
        //先把非数字的都替换掉，除了数字和.
        obj.value = obj.value.replace(/[^\d\.]/g,'');
        //必须保证第一个为数字而不是.
        obj.value = obj.value.replace(/^\./g,'');
        //保证只有出现一个.而没有多个.
        obj.value = obj.value.replace(/\.{2,}/g,'.');
        //保证.只出现一次，而不能出现两次以上
        obj.value = obj.value.replace('.','$#$').replace(/\./g,'').replace('$#$','.');
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
        //如果第一位是负号，则允许添加
        if(t == '-'){
            return ;
        }
    }
    function deleteJizhang(id){
        $.confirm("您确定要删除吗？", function() {
            $.post("/meetingSign/jizhang/deleteJizhang", {id: id}, function (data, status) {
                    if(data=="fail"){
                        $.toptip('删除失败', 'error');
                    }
                    else{
                        $.toptip('删除成功', 'success');
                        document.getElementById("zhangdanDiv_"+id).innerHTML=  "";
                    }
            });
        }, function() {
            //点击取消后的回调函数
        });

    }
    function picker(id){
        console.log(id);
        $("#picker_"+id).picker({
            title: "请选择消费类型",
            cols: [
                {
                    textAlign: 'center',
                    values: ['日常消费', '餐饮消费', '娱乐消费', '网购消费', '转账消费', '交通消费', '租房消费', '水电消费', '服饰消费', '其他消费']
                }
            ]
        });
    }
    function formatDate2(now) {
        console.log(now);
        var year=now.getFullYear();
        var month=now.getMonth()+1;
        var date=now.getDate();
        var hour=now.getHours();
        var minute=now.getMinutes();
        return year+"-"+month+"-"+date+"T"+hour+":"+minute;
    }
    function quxiao(id,account,type,date,content,time){

       var  content = '<div class="weui-form-preview">' +
                '    <div class="weui-form-preview__hd">' +
                '        <label class="weui-form-preview__label">消费金额</label>' +
                '        <em class="weui-form-preview__value"><div id="account_'+id.trim()+'">¥' +account.trim() + '</div></em>' +
                '    </div>' +
                '    <div class="weui-form-preview__bd">' +
                '        <div class="weui-form-preview__item">' +
                '            <label class="weui-form-preview__label">记账类型</label>' +
                '            <span class="weui-form-preview__value"><div id="type_'+id.trim()+'"> ' + type.trim() + '</div></span>' +
                '        </div>' +
                '        <div class="weui-form-preview__item">' +
                '            <label class="weui-form-preview__label">消费时间</label>' +
                '            <span class="weui-form-preview__value"><div id="time_'+id.trim()+'" hidden>'+time.trim()+'</div><div id="date_'+id.trim()+'"> ' + date.trim() + '</div></span>' +
                '        </div>' +
                '        <div class="weui-form-preview__item">' +
                '            <label class="weui-form-preview__label">备注</label>' +
                '            <span class="weui-form-preview__value"><div id="content_'+id.trim()+'"> ' + content.trim() + '</div></span>' +
                '        </div>' +
                '    </div>' +
                '    <div class="weui-form-preview__ft" id="edit_'+id.trim()+'">' +
                '        <a class="weui-form-preview__btn weui-form-preview__btn_default" onclick="deleteJizhang(\''+id.trim()+'\')">删除</a>' +
                '     <button type="submit" class="weui-form-preview__btn weui-form-preview__btn_primary" onclick="alertForm(\''+id.trim()+'\')">编辑</button>' +
                '    </div>' +
                '</div><br>';
            document.getElementById("zhangdanDiv_"+id).innerHTML=  content;
    }
    function saveForm(id,account,type,date,content) {
        var account = document.getElementById("money_"+id).value;
        var type =  document.getElementById("picker_"+id).value;
        var time = document.getElementById("choseTime_"+id).value;
        if(time.trim()==""){   //未填写。
            time = document.getElementById("time_"+id).innerHTML;
            time = formatDate2(new Date(time-0));
        }
        var content =document.getElementById("eventContent"+id).value;
        console.log(content);
        $.post("/meetingSign/jizhang/saveJizhang", {
            id: id,
            account: account,
            type: type,
            moneyTime: time,
            content: content
        }, function (data, status) { //响应
            var newtime = data.moneyTime;
            var temp = newtime-0;
            var date = formatDate(new Date(temp));
           document.getElementById("zhangdanDiv_"+id).innerHTML =   '<div class="weui-form-preview">' +
                   '    <div class="weui-form-preview__hd">' +
                   '        <label class="weui-form-preview__label">消费金额</label>' +
                   '        <em class="weui-form-preview__value"><div id="account_'+id+'">¥' +account + '</div></em>' +
                   '    </div>' +
                   '    <div class="weui-form-preview__bd">' +
                   '        <div class="weui-form-preview__item">' +
                   '            <label class="weui-form-preview__label">记账类型</label>' +
                   '            <span class="weui-form-preview__value"><div id="type_'+id+'"> ' + type + '</div></span>' +
                   '        </div>' +
                   '        <div class="weui-form-preview__item">' +
                   '            <label class="weui-form-preview__label">消费时间</label>' +
                   '            <span class="weui-form-preview__value"><div id="time_'+id+'" hidden>'+newtime+'</div><div id="date_'+id+'"> ' +date + '</div></span>' +
                   '        </div>' +
                   '        <div class="weui-form-preview__item">' +
                   '            <label class="weui-form-preview__label">备注</label>' +
                   '            <span class="weui-form-preview__value"><div id="content_'+id+'"> ' + content + '</div></span>' +
                   '        </div>' +
                   '    </div>' +
                   '    <div class="weui-form-preview__ft" id="edit_'+id+'">' +
                   '        <a class="weui-form-preview__btn weui-form-preview__btn_default" onclick="deleteJizhang(\''+id+'\')">删除</a>' +
                   '     <button type="submit" class="weui-form-preview__btn weui-form-preview__btn_primary" onclick="alertForm(\''+id+'\')">编辑</button>' +
                   '    </div>' +
                   '</div>';
        });
    }
    var areYouAlert = "1";
    function alertTips(){
        if(areYouAlert==1){
            $.modal({
                title: "Nan:小提示",
                text: "如无需修改时间请勿操作时间框<br>系统将自动保存原来的时间:<br>",
                buttons: [
                    { text: "不再提示", onClick: function(){areYouAlert = 0;  } },
                    { text: "确定", className: "default", onClick: function(){ } },
                ]
            });
        }else{
            console.log("已经不再提示");
        }
    }
    function alertForm(id){
       var account = document.getElementById("account_"+id).innerHTML;
       var type =  document.getElementById("type_"+id).innerHTML;
       var date = document.getElementById("date_"+id).innerHTML;
       var time = document.getElementById("time_"+id).innerHTML;
       var content =document.getElementById("content_"+id).innerHTML;
        account = account.substr(1);
        time = time-0;
        document.getElementById("account_"+id).innerHTML = " <input type=\"text\" class=\"input\" id=\"money_"+id+"\" name=\"account\" value=\""+account+"\"  onkeyup=\"onlyNumber(this)\">"
        document.getElementById("type_"+id).innerHTML ="<input type=\"text\" id='picker_"+id+"' class=\"input\" style=\"background-color: white\"  value=\""+type.trim()+"\" name=\"type\" onclick=\"picker('"+id+"')\" readonly=true/>";
        document.getElementById("date_"+id).innerHTML ="<input class=\"input weui-input\" type=\"datetime-local\" name=\"moneyTime\" id=\"choseTime_"+id+"\" style=\"background-color: white;height: 35px;border:1px solid #CCC;margin: 3px auto\">";
        document.getElementById("content_"+id).innerHTML ="<textarea type=\"text\" class=\"input\" id=\"eventContent"+id+"\" name=\"content\" maxlength=\"500\" data-validate=\"\" placeholder=\"请输入事件信息\">"+content.trim()+"</textarea>";
        document.getElementById("edit_"+id).innerHTML = ' <a class="weui-form-preview__btn weui-form-preview__btn_default" onclick="deleteJizhang(\''+id+'\')">删除</a>' +
                '<a class="weui-form-preview__btn weui-form-preview__btn_default" onclick="quxiao(\''+id+'\',\''+account+'\',\''+type+'\',\''+date+'\',\''+content+'\',\''+time+'\')">取消</a>' +
                '<button type="submit" class="weui-form-preview__btn weui-form-preview__btn_primary" onclick="saveForm(\''+id+'\',\''+account+'\',\''+type+'\',\''+formatDate2(new Date(time))+'\',\''+content+'\')">保存</button>' ;
        alertTips(areYouAlert);
    }
    function formatDate(now) {
        var year=now.getFullYear();
        var month=now.getMonth()+1;
        var date=now.getDate();
        var hour=now.getHours();
        var minute=now.getMinutes();
        var second=now.getSeconds();
        return year+"年"+month+"月"+date+"日 "+hour+"时"+minute+"分"+second+"秒";
    }
    reloadMyJizhang();
    function reloadMyJizhang() {
        var size = document.getElementById("size").value;
        var page = document.getElementById("page").value;
        $.post("/meetingSign/jizhang/reloadMyJizhang", {size: size, page: page}, function (data, status) {  //先默认拿第0页两条
            var eventList = data.eventList;
            var allcontent = "";
            for (var i = 0; i < eventList.length; i++) {
                allcontent = allcontent + '<div id="zhangdanDiv_'+eventList[i].id+'">' +
                        '<div class="weui-form-preview">' +
                        '    <div class="weui-form-preview__hd">' +
                        '        <label class="weui-form-preview__label">消费金额</label>' +
                        '        <em class="weui-form-preview__value"><div id="account_'+eventList[i].id+'">¥' + eventList[i].account + '</div></em>' +
                        '    </div>' +
                        '    <div class="weui-form-preview__bd">' +
                        '        <div class="weui-form-preview__item">' +
                        '            <label class="weui-form-preview__label">记账类型</label>' +
                        '            <span class="weui-form-preview__value"><div id="type_'+eventList[i].id+'"> ' + eventList[i].type + '</div></span>' +
                        '        </div>' +
                        '        <div class="weui-form-preview__item">' +
                        '            <label class="weui-form-preview__label">消费时间</label>' +
                        '            <span class="weui-form-preview__value"><div id="time_'+eventList[i].id+'" hidden>'+eventList[i].moneyTime+'</div><div id="date_'+eventList[i].id+'"> ' + formatDate(new Date(eventList[i].moneyTime)) + '</div></span>' +
                        '        </div>' +
                        '        <div class="weui-form-preview__item">' +
                        '            <label class="weui-form-preview__label">备注</label>' +
                        '            <span class="weui-form-preview__value"><div id="content_'+eventList[i].id+'"> ' + eventList[i].content + '</div></span>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="weui-form-preview__ft" id="edit_'+eventList[i].id+'">' +
                        '        <a class="weui-form-preview__btn weui-form-preview__btn_default" onclick="deleteJizhang(\''+eventList[i].id+'\')">删除</a>' +
                        '     <button type="submit" class="weui-form-preview__btn weui-form-preview__btn_primary" onclick="alertForm(\''+eventList[i].id+'\')">编辑</button>' +
                        '    </div>' +
                        '</div></div><br>';
            }
            document.getElementById("size").value = data.size;
            document.getElementById("page").value = data.page-0+1;
            var content = document.getElementById("tableDiv").innerHTML;
            document.getElementById("tableDiv").innerHTML = content + allcontent;
            if (data.isHasPage == "1") {
                document.getElementById("pageJizhang").innerHTML = '<button class="button bg" style="width: 100%" onclick="reloadMyJizhang()">点击加载更多</button>';
            }
            else {
                document.getElementById("pageJizhang").innerHTML = '<button class="button bg" style="width: 100%">已无更多内容</button>';
            }
        });
    }
</script>
</html>
