<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>开始我的消费</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <link rel="stylesheet" href="/meetingSign/css/pintuer.css">
    <link rel="stylesheet" href="http://cdn.bootcss.com/weui/0.4.3/style/weui.min.css">
    <link rel="stylesheet" href="http://cdn.bootcss.com/jquery-weui/0.8.3/css/jquery-weui.min.css">
    <script src="/meetingSign/js/jquery.js"></script>
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script>
        function onlyNumber(obj){
            //得到第一个字符是否为负号
            var t = obj.value.charAt(0);
            //先把非数字的都替换掉，除了数字和.
            obj.value = obj.value.replace(/[^\d\.]/g,'');
            //必须保证第一个为数字而不是.
            obj.value = obj.value.replace(/^\./g,'');
            //保证只有出现一个.而没有多个.
            obj.value = obj.value.replace(/\.{2,}/g,'.');
            //保证.只出现一次，而不能出现两次以上
            obj.value = obj.value.replace('.','$#$').replace(/\./g,'').replace('$#$','.');
            obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
            //如果第一位是负号，则允许添加
            if(t == '-'){

                return;
            }
        }
    </script>
</head>

<body>
<div class="alert alert-blue" style="text-align: center;font-size: 13px">
    点击头像可查看已记的账单
</div>
<div id="main" style="text-align: center">
    <a href="/meetingSign/jizhang/myJizhang?openid=${openid}&imgHead=${imgHead}"><img src="${imgHead}" width="100" height="100" class="img-border radius-circle"style="margin: 0px;text-align: center"/></a>
    <form method="post" action="/meetingSign/jizhang/successEvent" id="eventForm">
        <div class="demo" style="text-align: center">
            <input hidden type="text" value="${openid}" name="openid"/>
            <input hidden type="text" value="${imgHead}" name="imgHead"/>
            <input hidden type="text" value="${nickname}" name="nickname"/>

            <div class="form-group" id="f_1508773867876">
                <div class="label">
                    <label for="f_currency_txt">
                        消费时间
                    </label>
                </div>
                <div class="field">
                <#--<input type="text" class="input" id="f_currency_txt" name="f_currency_txt" maxlength="10" value="" data-validate="required:请填写月收入,currency:请输入月收入，1到99岁,length#<10:字数在0-10个" placeholder="输入月收入">-->
                    <input class="input weui-input" type="datetime-local" name="moneyTime" value="" id="choseTime" placeholder="" style="background-color: white">
                </div>
            </div>

            <div class="label">
                <label for="f_currency_txt">
                    消费类型
                </label>
            </div>
            <input type="text" id='picker' class="input" style="background-color: white" name="type"/>
            <div class="form-group" id="f_1519916189451">
                <div class="label">
                    <label for="f_currency_txt">
                        消费金额
                    </label>
                </div>
                <div class="field">
                    <input type="text" class="input" id="money" name="account" value="" placeholder="输入金额(保留两位小数)"  onkeyup="onlyNumber(this)">
                </div>
            </div>


            <div class="form-group" id="f_1506608081185">
                <div class="label">
                    <label for="f_address_txt">
                        备注
                    </label>
                </div>
                <div class="field">
                    <textarea type="text" class="input" id="eventContent" name="content" maxlength="500" value="" data-validate="" placeholder="请输入事件信息"></textarea>
                </div>


            </div>
        </div>
    </form>
    <div style="text-align: center">
        <button class="button button-big border-sub" id="submitForm"><span class="icon-calendar"></span>  提交</button>
    </div>
</div>
</div>
</body>
<!-- body 最后 -->
<script src="http://cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/jquery-weui/0.8.3/js/jquery-weui.min.js"></script>
<script>

        window.onload = function(){
        document.getElementById("submitForm").onclick = function () {
            var choseTime =  document.getElementById("choseTime").value;
            var eventContent = document.getElementById("eventContent").value;
            var money = document.getElementById("money").value;
            var picker = document.getElementById("picker").value;
            if(choseTime.length<=0){
                $.alert("时间不能为空");
                return;
            }
            if(money.length<=0){
                $.alert("请填写消费的金额");
                return;
            }
            if(picker.length<=0){
                $.alert("请选择消费类型");
                return;
            }
            document.getElementById("eventForm").submit();
        }
         $("#picker").picker({
                    title: "请选择消费类型",
                    cols: [
                        {
                            textAlign: 'center',
                            values: ['日常消费', '餐饮消费', '娱乐消费', '网购消费', '转账消费', '交通消费', '租房消费', '水电消费', '服饰消费', '其他消费']
                        }
                    ]
                });


    }
</script>
</html>
