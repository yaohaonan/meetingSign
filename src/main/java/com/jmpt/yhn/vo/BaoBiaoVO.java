package com.jmpt.yhn.vo;

import lombok.Data;
import lombok.Getter;

import java.text.DecimalFormat;

@Getter
public class BaoBiaoVO {
    private String name;
    private Double allCount;
    private Double baifenbi;

    public void setAllCount(Double allCount) {
        this.allCount = allCount;
    }

    public void setBaifenbi(Double baifenbi) {
        DecimalFormat df = new DecimalFormat("0.00");
        this.baifenbi = Double.valueOf(df.format(baifenbi));
    }

    public void setName(String name) {
        this.name = name;
    }
}
