package com.jmpt.yhn.vo;
import com.jmpt.yhn.entity.Money;
import lombok.Data;

import java.util.List;
@Data
public class JizhangVO {
    private List<Money> eventList;
    private String page;
    private String size;
    private String isHasPage;
}
