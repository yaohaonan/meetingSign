package com.jmpt.yhn.dao;
import com.jmpt.yhn.entity.Money;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JizhangReponsitory extends JpaRepository<Money,String> {
    Page<Money> findByOpenid(String openid, Pageable pageable);  //分页查询
    Money findById(Integer id);
    Long countByOpenid(String openid);
    List<Money> findByOpenid(String openid);
    List<Money> findByType(String type);
}
