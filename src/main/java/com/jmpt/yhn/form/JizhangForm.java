package com.jmpt.yhn.form;

import lombok.Data;

import java.util.Date;
@Data
public class JizhangForm {
    private String content; //记账备注
    private String account;  //记账金额
    private String type; //记账类型
    private String moneyTime;  //消费时间
}
