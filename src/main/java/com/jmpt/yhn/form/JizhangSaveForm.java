package com.jmpt.yhn.form;

import lombok.Data;

@Data
public class JizhangSaveForm {
    private String id;
    private String content; //记账备注
    private String account;  //记账金额
    private String type; //记账类型
    private String moneyTime;  //消费时间
}
