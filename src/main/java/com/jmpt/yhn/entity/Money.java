package com.jmpt.yhn.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@DynamicUpdate    //自动更新时间字段
public class Money {
    @Id
    @GeneratedValue
    private Integer id;
    private String content; //记账备注
    private String account;  //记账金额
    private String type; //记账类型
    private Date moneyTime;  //消费时间
    private String openid;  //用户id;
    private Date createTime;   //创建时间
}
