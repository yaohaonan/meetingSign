package com.jmpt.yhn.service;

import com.jmpt.yhn.entity.Money;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;
import java.util.List;

public interface JizhangService {
    Page<Money> findByOpenid(String openid, Pageable pageable);  //分页查询
    Money saveMoney(Money money);
    Money updateMoney(Money money,String openid);
    String  deleteMoney(Integer id,String openid);
    Long countByOpenid(String openid);
    List<Money> findByOpenid(String openid);
    List<Money> findByType(String type);
}
