package com.jmpt.yhn.controller;

import com.jmpt.yhn.entity.Money;
import com.jmpt.yhn.entity.UserInfo;
import com.jmpt.yhn.enums.TypeEnum;
import com.jmpt.yhn.form.JizhangForm;
import com.jmpt.yhn.form.JizhangSaveForm;
import com.jmpt.yhn.service.JizhangService;
import com.jmpt.yhn.service.PushMessageService;
import com.jmpt.yhn.service.UserInfoService;
import com.jmpt.yhn.vo.BaoBiaoVO;
import com.jmpt.yhn.vo.JizhangVO;
import com.lly835.bestpay.rest.type.Get;
import com.lly835.bestpay.rest.type.Post;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/jizhang")
@Slf4j
@Data
public class JiZhangController {
    @Autowired
    private JizhangService jizhangService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private PushMessageService pushMessageService;
    @GetMapping("/index")   //首页
    public ModelAndView index(@RequestParam("openid") String openid,
                              @RequestParam("imgHead") String imgHead,
                              @RequestParam(value = "nickname", defaultValue = "") String nickname,
                              Map<String, Object> map) {
        map.put("openid", openid);
        map.put("imgHead", imgHead);
        map.put("nickname", nickname);
        return new ModelAndView("page/index3", map);
    }

    @PostMapping("/successEvent")
    public ModelAndView successEvent(@Valid JizhangForm jizhangForm, BindingResult bindingResult,
                                     Map<String, Object> map) throws Exception {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String sessionOpenid = (String) attributes.getRequest().getSession().getAttribute("openid");
        String moneyTime = jizhangForm.getMoneyTime().replace("T", " ") + ":00";
        System.out.println(jizhangForm.getMoneyTime());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        UserInfo userInfo = userInfoService.findOne(sessionOpenid);
        Money money = new Money();
        BeanUtils.copyProperties(jizhangForm, money);
        money.setCreateTime(new Date());
        money.setOpenid(sessionOpenid);
        money.setMoneyTime(sdf.parse(moneyTime));
        jizhangService.saveMoney(money);
        map.put("msg", "记账成功，正在返回原页面");
        map.put("url", "/meetingSign/jizhang/index?openid=" + userInfo.getOpenid() + "&imgHead=" + userInfo.getImgHead() + "&nickname=" + userInfo.getUsername());
        return new ModelAndView("common/success", map);
    }

    @GetMapping("/myJizhang")
    public ModelAndView myJizhang(@RequestParam("openid") String openid,
                                  @RequestParam("imgHead") String imgHead,
                                  @RequestParam(value = "nickname", defaultValue = "") String nickname,
                                  Map<String, Object> map) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String sessionOpenid = (String)attributes.getRequest().getSession().getAttribute("openid");
        List<Money> moneyList = jizhangService.findByOpenid(sessionOpenid);  //得到所有的数据;
        Double sellCount = 0.00;
        for (Money money : moneyList) {
            sellCount += Double.valueOf(money.getAccount());  //加总价
        }
        map.put("openid", openid);
        map.put("sellCount", sellCount);
        map.put("imgHead", imgHead);
        map.put("nickname", nickname);
        map.put("size", "5");
        map.put("page", "1");
        return new ModelAndView("page/myJizhang", map);
    }
    @PostMapping("/reloadMyJizhang")
    public JizhangVO reoloadManyContent(@RequestParam(value = "page") String page,
                                        @RequestParam(value = "size") String size,
                                        HttpServletResponse response) throws IOException {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String sessionOpenid = (String)attributes.getRequest().getSession().getAttribute("openid");
        response.setCharacterEncoding("UTF-8");
        log.info("【分页】,page={},size={}", page, size);
        PageRequest request = new PageRequest(Integer.valueOf(page) - 1, Integer.valueOf(size));
        Page<Money> result = jizhangService.findByOpenid(sessionOpenid, request);
        JizhangVO jizhangVO = new JizhangVO();
        jizhangVO.setEventList(result.getContent());
        jizhangVO.setPage(page);
        jizhangVO.setSize(size);
        if ((Integer.valueOf(page) - 1) < result.getTotalPages()) {
            jizhangVO.setIsHasPage("1"); //还有内容。
        } else {
            jizhangVO.setIsHasPage("0");
        }
        return jizhangVO;
    }

    @PostMapping("/deleteJizhang")
    public String deleteJizhang(@RequestParam(value = "id") String id
    ) throws IOException {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String sessionOpenid = (String)attributes.getRequest().getSession().getAttribute("openid");
        String result = jizhangService.deleteMoney(Integer.valueOf(id), sessionOpenid);
        return result;
    }

    @PostMapping("/saveJizhang")
    public Money saveJizhang(@Valid JizhangSaveForm jizhangSaveForm, BindingResult bindingResult,
                             Map<String, Object> map) throws Exception {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String sessionOpenid = (String)attributes.getRequest().getSession().getAttribute("openid");
        Money money = new Money();
        System.out.println(jizhangSaveForm);
        BeanUtils.copyProperties(jizhangSaveForm, money);
        String moneyTime = jizhangSaveForm.getMoneyTime().replace("T", " ") + ":00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        money.setMoneyTime(sdf.parse(moneyTime));
        money.setId(Integer.valueOf(jizhangSaveForm.getId()));
        Money money2 = jizhangService.updateMoney(money, sessionOpenid);
        return money2;
    }
    @GetMapping("/getBaoBiao")
    public void getBaoBiao(Map<String, Object> map) throws Exception {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String sessionOpenid = (String)attributes.getRequest().getSession().getAttribute("openid");
        pushMessageService.baobiao(sessionOpenid);
    }
    @GetMapping("/getAllData")
    public ModelAndView getAllData(@RequestParam("openid") String openid,
                                   @RequestParam("imgHead") String imgHead,
                                   Map<String, Object> map) throws Exception {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String sessionOpenid = (String)attributes.getRequest().getSession().getAttribute("openid");
        List<Money> moneyList = jizhangService.findByOpenid(sessionOpenid);  //得到所有的数据;
        Double sellCount = 0.00;
        //以下都是垃圾代码--->数据库设计写错了我的天。
        List<BaoBiaoVO> baoBiaoVOList = new ArrayList<>();
        Double richang = 0.00;
        Double canyin = 0.00;
        Double yule = 0.00;
        Double wanggou = 0.00;
        Double zhuanzhang = 0.00;
        Double jiaotong = 0.00;
        Double zufang = 0.00;
        Double shuidian = 0.00;
        Double fushi = 0.00;
        Double qita = 0.00;
        for (Money money : moneyList) {
            sellCount+=  Double.valueOf(money.getAccount());  //加总价
            if(money.getType().equals("日常消费")){
                richang +=Double.valueOf(money.getAccount());
            }
            if(money.getType().equals("餐饮消费")){
                canyin +=Double.valueOf(money.getAccount());
            }
            if(money.getType().equals("娱乐消费")){
                yule +=Double.valueOf(money.getAccount());
            }
            if(money.getType().equals("网购消费")){
                wanggou +=Double.valueOf(money.getAccount());
            }
            if(money.getType().equals("转账消费")){
                zhuanzhang +=Double.valueOf(money.getAccount());
            }
            if(money.getType().equals("交通消费")){
                jiaotong +=Double.valueOf(money.getAccount());
            }
            if(money.getType().equals("租房消费")){
                zufang +=Double.valueOf(money.getAccount());
            }
            if(money.getType().equals("水电消费")){
                shuidian+=Double.valueOf(money.getAccount());
            }
            if(money.getType().equals("服饰消费")){
                fushi +=Double.valueOf(money.getAccount());
            }
            if(money.getType().equals("其他消费")){
                qita +=Double.valueOf(money.getAccount());
            }
        }
        BaoBiaoVO baoBiaoVO1 = new BaoBiaoVO();baoBiaoVO1.setName("日常消费");baoBiaoVO1.setAllCount(richang);baoBiaoVO1.setBaifenbi(richang/sellCount);
        BaoBiaoVO baoBiaoVO2 = new BaoBiaoVO();baoBiaoVO2.setName("餐饮消费");baoBiaoVO2.setAllCount(canyin);baoBiaoVO2.setBaifenbi(canyin/sellCount);
        BaoBiaoVO baoBiaoVO3 = new BaoBiaoVO();baoBiaoVO3.setName("娱乐消费");baoBiaoVO3.setAllCount(yule);baoBiaoVO3.setBaifenbi(yule/sellCount);
        BaoBiaoVO baoBiaoVO4= new BaoBiaoVO();baoBiaoVO4.setName("网购消费");baoBiaoVO4.setAllCount(wanggou);baoBiaoVO4.setBaifenbi(wanggou/sellCount);
        BaoBiaoVO baoBiaoVO5 = new BaoBiaoVO();baoBiaoVO5.setName("转账消费");baoBiaoVO5.setAllCount(zhuanzhang);baoBiaoVO5.setBaifenbi(zhuanzhang/sellCount);
        BaoBiaoVO baoBiaoVO6 = new BaoBiaoVO();baoBiaoVO6.setName("交通消费");baoBiaoVO6.setAllCount(jiaotong);baoBiaoVO6.setBaifenbi(jiaotong/sellCount);
        BaoBiaoVO baoBiaoVO7 = new BaoBiaoVO();baoBiaoVO7.setName("租房消费");baoBiaoVO7.setAllCount(zufang);baoBiaoVO7.setBaifenbi(zufang/sellCount);
        BaoBiaoVO baoBiaoVO8 = new BaoBiaoVO();baoBiaoVO8.setName("水电消费");baoBiaoVO8.setAllCount(shuidian);baoBiaoVO8.setBaifenbi(shuidian/sellCount);
        BaoBiaoVO baoBiaoVO9 = new BaoBiaoVO();baoBiaoVO9.setName("服饰消费");baoBiaoVO9.setAllCount(fushi);baoBiaoVO9.setBaifenbi(fushi/sellCount);
        BaoBiaoVO baoBiaoVO10 = new BaoBiaoVO();baoBiaoVO10.setName("其他消费");baoBiaoVO10.setAllCount(qita);baoBiaoVO10.setBaifenbi(qita/sellCount);
        baoBiaoVOList.add(baoBiaoVO1);
        baoBiaoVOList.add(baoBiaoVO2);
        baoBiaoVOList.add(baoBiaoVO3);
        baoBiaoVOList.add(baoBiaoVO4);
        baoBiaoVOList.add(baoBiaoVO5);
        baoBiaoVOList.add(baoBiaoVO6);
        baoBiaoVOList.add(baoBiaoVO7);
        baoBiaoVOList.add(baoBiaoVO8);
        baoBiaoVOList.add(baoBiaoVO9);
        baoBiaoVOList.add(baoBiaoVO10);
        map.put("count",jizhangService.countByOpenid(sessionOpenid));
        map.put("sellCount",sellCount);
        map.put("ListMoneyData",baoBiaoVOList);
        return new ModelAndView("page/BaoBiao",map);

    }
}
