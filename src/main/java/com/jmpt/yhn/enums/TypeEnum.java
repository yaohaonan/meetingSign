package com.jmpt.yhn.enums;

import lombok.Getter;

@Getter
public enum TypeEnum {
    RICHANG(0, "日常消费"),
    CANYIN(1, "餐饮消费"),
    YULE(2, "娱乐消费"),
    WANGGOU(3, "网购消费"),
    ZHUANZHANG(4, "转账消费"),
    JIAOTONG(5, "交通消费"),
    ZUFANG(6, "租房消费"),
    SHUIDIAN(7, "水电消费"),
    FUZHI(8, "服饰消费"),
    QITA(9, "其他消费");
    private Integer code;
    private String message;

    TypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}